import os
from bottle import run
from app import app
import account, sendmail

HOST = os.environ.get('HOST') or 'localhost'
PORT = os.environ.get('PORT') or 8000
run(app, host=HOST, port=PORT)
