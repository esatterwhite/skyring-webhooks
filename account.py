import db, json, skyring
from bottle import request, response
from app import app
from collections import namedtuple

Account = namedtuple('Account','id, name, created_at, active, deactivated_at')
AccountTimer = namedtuple('AccoutTimer', 'id account, created_at, url')


LIST_ACCOUNT_SQL = '''
SELECT * FROM account
'''

@app.get('/account')
def get_account():
  c = db.connection.cursor()
  c.execute(LIST_ACCOUNT_SQL)
  rows = c.fetchall()
  response.content_type = 'application/json'
  return json.dumps([Account._make(r)._asdict() for r in rows])


GET_SQL = '''
SELECT * FROM account
WHERE id = ?
'''

INSERT_ACCOUNT_SQL = '''
INSERT INTO account (name) 
VALUES (?)
'''

INSERT_ACCOUNT_TIMER_SQL = '''
INSERT INTO account_timer (account, url)
VALUES (?, ?)
'''

@app.post('/account')
def post_account():
  c = db.connection.cursor()
  c.execute(INSERT_ACCOUNT_SQL, (request.json['name'],)) 
  c.fetchone()
  c.execute(GET_SQL, (c.lastrowid,))
  account = Account._make(c.fetchone())._asdict()
  record = json.dumps(account)

  data = {
    'subject': 'hello {0}'.format(account['name'])
    , 'to': 'mail@mail.com'
    , 'from': 'admin@skyhook.com'
    , 'body': 'thank you for signing up '
  }
  timer = skyring.create(data, 'http://localhost:8000/webhook/mail', timeout=10000)
  c.execute(INSERT_ACCOUNT_TIMER_SQL, (account['id'], timer))
  db.connection.commit()
  print('\n', timer, '\n')
  return record


LIST_TIMER_SQL = '''
SELECT * FROM account_timer
WHERE account = ?
'''

@app.get('/account/<id:int>/timer')
def get_account_timers(id):
  c = db.connection.cursor()
  c.execute(LIST_TIMER_SQL, (id,))
  rows = c.fetchall()
  response.content_type = 'application/json'
  return json.dumps([AccountTimer._make(r)._asdict() for r in rows])


@app.delete('/account/<id:int>/timer')
def cancel_account_timer(id):
  SQL = "{0} LIMIT 1".format(LIST_TIMER_SQL)
  c = db.connection.cursor()
  c.execute(SQL, (id, ))
  row = c.fetchone()
  if row:
    record = AccountTimer._make(row)._asdict()
    response.status = skyring.cancel(record['url'])
    c.execute('DELETE from account_timer where id = ?', (id,))
    db.connection.commit()
  else:
    response.status = 404
  return

