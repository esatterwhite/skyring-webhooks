"""
create_account
"""

from yoyo import step

__depends__ = {}

steps = [
    step(
    ''' 
    CREATE TABLE account (
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
    , name VARCHAR(100)
    , created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
    , active BOOLEAN default FALSE NOT NULL
    , deactivated_at DATETIME NULL
    )
    ''',
    
    '''
    DROP TABLE account
    '''
    )
]
