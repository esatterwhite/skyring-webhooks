"""
create_account_timer
"""

from yoyo import step

__depends__ = {'20170107_01_fEWtH-create-account'}

steps = [
    step(
    '''
    CREATE TABLE account_timer (
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
    , account INTEGER NOT NULL REFERENCES account(id)
    , created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
    , url TEXT NOT NULL
    )
    ''',
    '''
    DROP TABLE account_timer
    '''
    )
]
