import smtplib, os, settings
from email.mime.text import MIMEText
from bottle import request, response
from app import app

SMTP_HOST = settings.SMTP_HOST 
SMTP_PORT = settings.SMTP_PORT


@app.post('/webhook/mail')
def send_mail():
  payload = request.json
  msg = MIMEText(payload['body'])
  msg['Subject'] = payload['subject']
  msg['From'] = payload['from']
  msg['To'] = payload['to']
  print(msg.as_string())
  response.status = 200
  return 
