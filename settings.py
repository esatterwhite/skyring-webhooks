import os

BASE_DIR = os.path.abspath(os.path.curdir)
DB_NAME = os.environ.get('DB_NAME') or 'hooks.db'
DB_PATH = os.path.join(BASE_DIR, DB_NAME)
SKYRING_URL = os.environ.get('SKYRING_URL') or 'http://localhost:2300'
SMTP_HOST = os.environ.get('SMTP_HOST') or 'localhost'
SMTP_PORT = os.environ.get('SMTP_PORT') or 25
