import json, os, settings
from bottle import response
from urllib.request import Request, urlopen
from urllib.error import HTTPError

def create(data, callback, timeout=5000, method='POST'):
  URL = "{0}/{1}".format(settings.SKYRING_URL, "timer")
  payload = {
    "timeout": timeout
  , "data": data
  , "callback": {
      "transport": "http"
    , "method": method
    , "uri": callback
    }
  }
  req = Request(url=URL, data=bytes(json.dumps(payload),'ascii'), method=method)
  res = urlopen(req)
  return res.headers['location']

def cancel(uri):
  URL = "{0}{1}".format(settings.SKYRING_URL, uri)
  req = Request(url=URL, method='DELETE')
  try:
    res = urlopen(req)
  except HTTPError as err:
    return err.code 
  return res.status
